package analyze

import (
	"encoding/json"
	"fmt"
	"gitlab.com/agustin.delpino/caffe-test/cmd/flags"
	"gitlab.com/agustin.delpino/caffe-test/pkg/ct"
	"gopkg.in/yaml.v3"
)

func MarshalSuiteTests(o flags.Output, s []*ct.SuiteTest) ([]byte, error) {
	switch o {
	case flags.JsonFmt:
		return json.MarshalIndent(s, "", "  ")
	case flags.YmlFmt:
		return yaml.Marshal(s)
	case flags.TextFmt:
		return ct.TextMarshal(s), nil
	}
	return nil, fmt.Errorf("%s is an invalid output format", o)
}
