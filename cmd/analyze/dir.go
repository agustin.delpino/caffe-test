package analyze

import (
	"github.com/spf13/cobra"
	"gitlab.com/agustin.delpino/caffe-test/cmd/flags"
	"gitlab.com/agustin.delpino/caffe-test/pkg/ct"
	"strings"
)

func getDirCmd() *cobra.Command {
	var ignore []string

	c := &cobra.Command{
		Use:   "dir DIRNAME",
		Args:  cobra.ExactArgs(1),
		Short: "allows to analyze a entire directory",
		RunE: func(cmd *cobra.Command, args []string) error {
			var p string
			if ignore != nil && len(ignore) > 0 {
				p = strings.Join(ignore, "|")
			}

			dt, err := ct.Detect(ct.InspectDirectory(args[0], p))

			if err != nil {
				cmd.PrintErr("detection process failed")
				return err
			}

			b, bErr := MarshalSuiteTests(flags.OutputFlag, ct.Analyze(dt))
			if bErr != nil {
				cmd.PrintErr("marshal process failed")
				return bErr
			}

			cmd.Println(string(b))
			return nil
		},
	}

	c.Flags().StringArrayVarP(&ignore, "ignore", "i", nil, "adds a file pattern to ignore")

	return c
}
