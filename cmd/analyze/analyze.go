package analyze

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/agustin.delpino/caffe-test/cmd/flags"
)

func GetAnalyzeCmd() *cobra.Command {
	c := &cobra.Command{
		Use:   "analyze",
		Short: "allows to analyze the source code for suggest test",
	}

	c.AddCommand(getFileCmd())
	c.AddCommand(getDirCmd())

	c.PersistentFlags().VarP(&flags.OutputFlag, "output", "o",
		fmt.Sprintf("sets the output format (%s)", flags.OutputFlag))

	return c
}
