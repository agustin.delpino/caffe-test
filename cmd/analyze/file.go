package analyze

import (
	"github.com/spf13/cobra"
	"gitlab.com/agustin.delpino/caffe-test/cmd/flags"
	"gitlab.com/agustin.delpino/caffe-test/pkg/ct"
)

func getFileCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "file FILENAME",
		Args:  cobra.ExactArgs(1),
		Short: "allows to analyze a go file",
		RunE: func(cmd *cobra.Command, args []string) error {
			dt, err := ct.Detect(ct.InspectFile(args[0]))
			if err != nil {
				cmd.PrintErr("detection process failed")
				return err
			}

			b, bErr := MarshalSuiteTests(flags.OutputFlag, ct.Analyze(dt))
			if bErr != nil {
				cmd.PrintErr("marshal process failed")
				return bErr
			}

			cmd.Println(string(b))
			return nil
		},
	}
}
