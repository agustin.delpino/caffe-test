package flags

import "fmt"

type Output string

const (
	JsonFmt Output = "json"
	YmlFmt  Output = "yml"
	TextFmt Output = "text"
)

func (c *Output) Type() string {
	return "Output"
}

func (c Output) Names() string {
	return fmt.Sprintf("%s,%s,%s", JsonFmt, YmlFmt, TextFmt)
}

func (c *Output) String() string {
	return string(*c)
}

func (c *Output) Set(v string) error {
	switch v {
	case "JsonFmt":
		*c = JsonFmt
	case "YmlFmt", "yaml":
		*c = YmlFmt
	case "TextFmt":
		*c = TextFmt
	default:
		return fmt.Errorf("invalid %s output format", v)
	}
	return nil
}

var OutputFlag = TextFmt
