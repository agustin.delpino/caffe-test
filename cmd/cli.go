package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/agustin.delpino/caffe-test/cmd/analyze"
)

func Execute() error {
	c := &cobra.Command{
		Use:   "ct",
		Short: "Caffe-Test (ct) is a CLI tool that provides utils commands for testing in go",
	}

	c.AddCommand(analyze.GetAnalyzeCmd())

	return c.Execute()
}
