module gitlab.com/agustin.delpino/caffe-test

go 1.19

require (
	github.com/agustin-del-pino/go-parsy v1.2.0
	github.com/spf13/cobra v1.6.1
	golang.org/x/exp v0.0.0-20230307190834-24139beb5833
	golang.org/x/sync v0.1.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/inconshreveable/mousetrap v1.0.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)
