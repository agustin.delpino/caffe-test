package ct

import (
	"fmt"
	"strings"
)

type StrBuilder struct {
	strings.Builder
}

func (s *StrBuilder) WriteStringLN(f string, a ...any) (int, error) {
	return s.WriteString(fmt.Sprintf(f+"\n", a...))
}

func (s *StrBuilder) Bytes() []byte {
	return []byte(s.String())
}

func TextMarshal(st []*SuiteTest) []byte {
	sb := &StrBuilder{}

	for _, s := range st {
		sb.WriteStringLN("# %s's Test", s.Name)
		if len(s.Tests) == 0 {
			sb.WriteStringLN("*no possible tests were detected*")
			sb.WriteStringLN("")
		}
		for _, t := range s.Tests {
			sb.WriteStringLN("## Test for %s", t.FunctionName)
			sb.WriteStringLN("Suggested name: **%s**.", t.GetSuggestedName())
			sb.WriteStringLN("Where detected: %v cases of return.", t.NumberOfReturns)
			sb.WriteStringLN("**There must be %v test cases for minimum coverage**", t.NumberOfReturns)
			sb.WriteStringLN("")
		}
	}

	return sb.Bytes()
}
