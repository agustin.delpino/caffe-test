package ct

import (
	"context"
	"fmt"
	"github.com/agustin-del-pino/go-parsy/pkg/parsy"
	"golang.org/x/sync/errgroup"
	"os"
	"path/filepath"
)

// CodeInspector is function that can execute a parsy.Inspector and must forward any error.
type CodeInspector func(i *parsy.Inspector) error

// InspectFile returns a CodeInspector ready for inspect a single source code file.
func InspectFile(p string) CodeInspector {
	return func(i *parsy.Inspector) error {
		return parsy.ParseFile(i, p)
	}
}

// InspectDirectory returns a CodeInspector ready for inspect an entire (sub-dirs) directory.
// This function executes each parsy.Inspector in goroutines and waits until end or one of them end with an error.
// It can provide an additional path pattern to ignore.
func InspectDirectory(d string, p string) CodeInspector {
	return func(i *parsy.Inspector) error {
		gErr, ctx := errgroup.WithContext(context.Background())

		ls, err := os.ReadDir(d)

		if err != nil {
			return err
		}

		for _, l := range ls {
			gErr.Go(func(l os.DirEntry) func() error {
				return func() error {
					n := fmt.Sprintf("%s/%s", d, l.Name())
					if p != "" {
						m, mErr := filepath.Match(p, filepath.Clean(l.Name()))
						if mErr != nil {
							return mErr
						}

						if m {
							return nil
						}
					}

					inErr, _ := errgroup.WithContext(ctx)

					if !l.IsDir() {
						inErr.Go(func() error {
							return InspectFile(n)(i)
						})
					} else {
						inErr.Go(func() error {
							return InspectDirectory(n, p)(i)
						})
					}
					return inErr.Wait()
				}
			}(l))
		}
		return gErr.Wait()
	}
}
