package ct

import "fmt"

// UnitTest is the unit piece of the Test.
// The relation is one unit-test per function/method.
type UnitTest struct {
	StructName      string `json:"structName" yaml:"struct-name"`
	FunctionName    string `json:"functionName" yaml:"function-name"`
	IsMethod        bool   `json:"isMethod" yaml:"is-method"`
	NumberOfReturns int    `json:"numberOfReturns" yaml:"number-of-returns"`
}

func (u *UnitTest) GetSuggestedName() string {
	if u.IsMethod {
		return fmt.Sprintf("Test%s_%s", u.StructName, u.FunctionName)
	} else {
		return fmt.Sprintf("Test%s", u.FunctionName)
	}

}

// SuiteTest contains the unit-test related between them.
type SuiteTest struct {
	Name  string      `json:"name" yaml:"name"`
	Tests []*UnitTest `json:"tests" yaml:"tests"`
}

// Analyze returns a slice of SuiteTest based on a Detector.
func Analyze(d *Detector) []*SuiteTest {
	st := []*SuiteTest{
		{
			Name:  "Default",
			Tests: []*UnitTest{},
		},
	}

	c := map[string]int{
		"default": 0,
	}

	i := 1
	for _, fi := range d.info {
		var s *SuiteTest

		t := &UnitTest{
			FunctionName: fi.Name,
		}

		if fi.Receivers != nil && len(fi.Receivers) > 0 {
			n := fi.Receivers[0].Ident.Name
			if ci, ok := c[n]; ok {
				s = st[ci]
			} else {
				s = &SuiteTest{
					Name:  n,
					Tests: []*UnitTest{},
				}
				st = append(st, s)
				c[n] = i
				i++
			}

			t.IsMethod = true
			t.StructName = n

		} else {
			s = st[c["default"]]
		}

		if fi.Returns != nil {
			t.NumberOfReturns = len(fi.Returns.ReturnCases)
		}

		s.Tests = append(s.Tests, t)
	}

	return st
}
