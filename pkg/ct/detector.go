package ct

import (
	"github.com/agustin-del-pino/go-parsy/pkg/parsy"
	"go/ast"
)

// Detector holds the information of detected functions/methods.
type Detector struct {
	info []*parsy.FuncInfo
}

// Detect returns a Detector instance which contains information of detected function/method.
// It must provide the CodeInspector for execute the detector.
// If the CodeInspector ends unsuccessfully it will return the error.
func Detect(c CodeInspector) (*Detector, error) {
	ins := parsy.NewInspector(&parsy.InspectorOptions{AvoidNil: true})
	dt := &Detector{
		info: []*parsy.FuncInfo{},
	}

	ins.FuncDecl.AddListener(func(fn *ast.FuncDecl) {
		dt.info = append(dt.info, parsy.GetFuncInfo(fn))
	})

	err := c(ins)

	if err != nil {
		return nil, err
	}

	return dt, nil
}
