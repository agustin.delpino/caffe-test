# Caffe-Test
CLI tool that provides utils commands for testing in go.

# Overview

Once downloaded the source code, at root, execute this line
for install the CLI as `caffe-test` command.
````shell
go install
````

Then, just execute over a go project.

````shell
caffe-test analyze file ./main.go > ./result.md
````

# Commands

## Analyze
Holds the commands for analyze code and suggests possible tests.

**Output Format Flag**
This flag `--output` `-o`, sets the output format for the resulted information.

- json
- yml, yaml
- text *(default)*

### File
This subcommand allows to analyze a single source code file.

````shell
caffe-test analyze file ./main.go
````

### Dir
This subcommand allows to analyze an entire directory.

````shell
caffe-test analyze dir ./
````

**Ignore Pattern Flag**
This flag `--ignore` `-i` add a path pattern for ignore.

```shell
caffe-test analyze dir ./ -i *_test.go
```


